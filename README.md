#GFramework.BlankWindow

1. 一个干干净净的WPF Window。
2. 最小化，最大化，关闭统统都没有，想怎么画就怎么画。 
3. 使用WindowChrome相关类库，一个真正BorderlessWindow。 
4. 未使用AllowTransparency 属性，你懂的。 
5. 窗口部分代码提取自 mahapps.metro。

##一个开源的UI控件集喜欢的可以去看 http://mahapps.com/。